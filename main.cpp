#include "benchmark/benchmark.h"
#include <iostream>
#include <vector>
#include <list>
#include <array>

uint64_t sum_of_vector_squared_random(int num_entries);
uint64_t sum_of_vector_squared(int num_entries);
uint64_t sum_of_list_squared(int num_entries);

static void random_vector_benchmark(benchmark::State &myfunction)
{
    while(myfunction.KeepRunning())
        auto val = sum_of_vector_squared_random(myfunction.range(0));
}

static void vector_benchmark(benchmark::State &myfunction)
{
    while(myfunction.KeepRunning())
        auto val = sum_of_vector_squared(myfunction.range(0));
}

static void list_benchmark(benchmark::State &myfunction)
{
    while(myfunction.KeepRunning())
        auto val = sum_of_list_squared(myfunction.range(0));
}

BENCHMARK(random_vector_benchmark)->Range(1, 10000);
BENCHMARK(vector_benchmark)->Range(1, 1000);
BENCHMARK(list_benchmark)->Range(1, 1000);
BENCHMARK_MAIN();

uint64_t sum_of_vector_squared_random(int num_entries)
{
    uint64_t sum = 0;
    std::vector<uint64_t>::iterator iter;
    std::vector<uint64_t> random_numbers;
    std::vector<uint64_t> square_numbers;

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        random_numbers.push_back(rand());
    }
    iter = random_numbers.begin();

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        square_numbers.push_back(*iter++ ^ 2);
    }
    iter = random_numbers.begin();

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        sum += *iter++;
    }
    return sum;
}
uint64_t sum_of_vector_squared(int num_entries)
{
    uint64_t sum = 0;
    std::vector<uint64_t>::iterator iter;
    std::vector<uint64_t> random_numbers;
    std::vector<uint64_t> square_numbers;

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
        random_numbers.push_back(loop_count);
    iter = random_numbers.begin();

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        square_numbers.push_back(*iter ^ 2);
        ++iter;
    }
    iter = random_numbers.begin();

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        sum += *iter;
        ++iter;
    }
    return sum;
}
uint64_t sum_of_list_squared(int num_entries)
{
    uint64_t sum = 0;
    std::list<uint64_t>::iterator iter;
    std::list<uint64_t> random_numbers;
    std::list<uint64_t> square_numbers;

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
        random_numbers.push_back(loop_count);
    iter = random_numbers.begin();

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        square_numbers.push_back(*iter ^ 2);
        ++iter;
    }
    iter = random_numbers.begin();

    for (int loop_count = 0; loop_count < num_entries; ++loop_count)
    {
        sum += *iter;
        ++iter;
    }
    return sum;
}
